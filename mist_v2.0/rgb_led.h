#ifndef RGBLED_H
#define RGBLED_H

#include <FastLED.h>

#define LED_PIN     25
//#define LED_TYPE    WS2812
//#define COLOR_ORDER GRB
#define NUM_LEDS    4
CRGB leds[NUM_LEDS];

#define BRIGHTNESS          50
TaskHandle_t  run_led_handle = NULL;

void rgb_led_setup();
void sinelon();
void run_led(void * para);
void set_color(char*);
void suspend_led();
void resume_led();

#endif
