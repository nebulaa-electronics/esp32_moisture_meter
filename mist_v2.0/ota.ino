#include "ota.h"

void ota_setup()
{
//  Serial.println("start ota");
  WiFi.mode(WIFI_STA);
  essid = read_String(10).c_str();
  //read_String(10).toCharArray(ssid,read_String(10).length()+1);
  essid.toCharArray(ssid,essid.length()+1);
  char_print(ssid);
  epass = read_String(30).c_str();
  //read_String(30).toCharArray(password,read_String(30).length()+1);
  epass.toCharArray(password,epass.length()+1);
  //char_print(password);
  WiFi.begin(essid.c_str(), epass.c_str());
  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
//  Serial.println("complete ota");
}
