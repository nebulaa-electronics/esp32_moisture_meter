#include "fdc.h"
#include "ble.h"
#include "load_cell.h"
#include "lm35.h"
#include "ota.h"
#include "rgb_led.h"
#include "mm_eeprom.h"
#include "batt.h"

const unsigned long lc_eventInterval = 500;
unsigned long lc_previousTime = 0;
String ip_addr,aa;

void setup() {
  Serial.begin(115200);
  eeprom_init();
  rgb_led_setup();
  ble_init();
  load_cell_init();
  load_cell_tare();
  ota_setup();
  fdc_init();
  original_cap();
}

void loop() {

  if (Serial.available()>0) {
    aa= Serial.readString();
  }
  aa.trim();
  if(rxValue == "Crop" || aa == "Crop")
  {
    rxValue="";
    aa="";
//    for(int i=0;i<NUM_LEDS;i++)
//    {
//      leds[i] = CRGB::Green;
//      FastLED.show();
//    }
//    suspend_led();
    crop_data(total_cap(),bulk_density(),load_cell_weight());
    serial_print();
//    resume_led();
  }
  else if(rxValue == "Reset")
  {
    rxValue="";
    ESP.restart();
  }
  else if(rxValue == "T" || rxValue == "t")
  {
    suspend_led();
    suspend_ble();
    rxValue="";
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i] = CRGB::Purple;
      FastLED.show();
    }
    load_cell_tare();
    resume_led();
    resume_ble();
    Serial.println("tare received");
  }
  else if(rxValue == "Calibrate")
  {
    rxValue="";
    load_cell_calibrate();  
    //Serial.println("Calibration done");
  }
  else if(rxValue == "Reset Cap" || aa == "Reset Cap")
  {
    rxValue="";
    original_cap();
  }
  else if(rxValue == "Live Weight")
  {
    rxValue = "";
    crop_data(NULL,NULL,load_cell_weight());
    delay(10);
  }
  else if(rxValue == "Cap" || aa == "Cap")
  {
    float_print(total_cap());
    Serial.println(total_cap());
    delay(10);
  }
  else if(rxValue == "Get IP")
  {
    rxValue = "";
    ip_addr = WiFi.localIP().toString().c_str();
    ip_addr.toCharArray(ip,ip_addr.length()+1);
    char_print(ip);
    //Serial.println(ip_addr);
  }
  else if(rxValue == "Wifi")
  {
    suspend_led();
    suspend_ble();
    rxValue = "";
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i] = CRGB::Yellow;
      FastLED.show();
    }
    //Serial.println("Set wifi cred");
    set_wifi();
    resume_led();
    resume_ble();
  }
  else if(rxValue == "Get Wifi")
  {
    rxValue="";
    essid = read_String(10).c_str();
    essid.toCharArray(ssid,essid.length()+1);
    char_print(ssid);
  }
  else if(rxValue == "Batt")
  {
    rxValue="";
    float_print(batt_volt());
    char_print("+");
    float_print(batt_per());
  }
}
