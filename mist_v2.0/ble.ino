#include "ble.h"

void check_ble(void *para)
{
  for(;;)
  {
    while(!deviceConnected)
    {
      //delay(100);
      vTaskDelay(100/portTICK_PERIOD_MS);
      for(int i=0;i<NUM_LEDS;i++)
      {
        leds[i] = CRGB::Red;
        FastLED.show();
      }
      suspend_led();
      pServer->getAdvertising()->start(); // restart advertising
    }
    resume_led();
    vTaskDelay(5/portTICK_PERIOD_MS);
  }
}

void ble_init()
{
  BLEDevice::init("Mist-0001");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                    CHARACTERISTIC_UUID_TX,
                    BLECharacteristic::PROPERTY_READ   |
                    BLECharacteristic::PROPERTY_NOTIFY
                  );
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  pRxCharacteristic = pService->createCharacteristic(
                       CHARACTERISTIC_UUID_RX,
                      BLECharacteristic::PROPERTY_WRITE
                    );

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  xTaskCreate(check_ble,"BLE_CONN_CHECK",3000,NULL,1,&ble_handle);
  resume_ble();
}

void crop_data(float abc, float cdf, float ghi)
{
  char xyz[16],qvw[16],pqr[16];
  dtostrf(abc,6,4,xyz);
  dtostrf(cdf,6,4,qvw);
  dtostrf(ghi,6,3,pqr);
  strcat(xyz,"+");
  strcat(xyz,qvw);
  strcat(xyz,"+");
  strcat(xyz,pqr);
  pTxCharacteristic->setValue(xyz);
  pTxCharacteristic->notify();
}

void char_print(char* abc)
{
  pTxCharacteristic->setValue(abc);
  pTxCharacteristic->notify();
  delay(10);
}

void float_print(float abc)
{
  char xyz[16];
  dtostrf(abc,6,8,xyz);
  pTxCharacteristic->setValue(xyz);
  pTxCharacteristic->notify();
  delay(10);
}

void serial_print()
{
  Serial.print(total_cap(),5);
  Serial.print("+");
  Serial.print(bulk_density(),5);
  Serial.print("+");
  Serial.println(load_cell_weight(),4);
}

void suspend_ble()
{
  if(ble_handle != NULL)
  {
    vTaskSuspend(ble_handle);
  }
}

void resume_ble()
{
  if(ble_handle != NULL)
  {
    vTaskResume(ble_handle);
  }
}
