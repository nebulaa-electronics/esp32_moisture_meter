#include "fdc.h"
#include "ble.h"
#include "load_cell.h"

void fdc_init()
{
  Wire.begin();
  bool capOk = capsense.begin(0xF, 0x6, 0x5, false); //setup all four channels, autoscan with 4 channels, deglitch at 10MHz, external oscillator 
  if (capOk)  
  {
    //Serial.println("Sensor OK");  
  }
  else 
  {
    //Serial.println("Sensor Fail");
  }
  delay(100);
  original_cap();
  delay(100);
}

void original_cap()
{
  raw_code = capsense.getReading28(CH_NO);
  F_sensor_x = ((1 * REF_FREQ * raw_code)/(DIVISOR))/1000000;
  C_sensor = (1/(INDUCTOR*pow((2*3.14*F_sensor_x),2)))-C;
  char_print("C_sense = ");
  float_print(C_sensor);
}

float total_cap()
{
  raw_code = capsense.getReading28(CH_NO);
  delay(100);
  F_sensor_x = ((1 * REF_FREQ * raw_code)/(DIVISOR))/1000000;
  C_measured = (1/(INDUCTOR*pow((2*3.14*F_sensor_x),2)))-C;
  C_measured = C_measured - C_sensor;
  if(C_measured <0) C_measured = 0;
  return C_measured;
}

float bulk_density()
{
  grain_weight = load_cell_weight();
  bulk_dens = (grain_weight/VOLUME);
  return bulk_dens;
}
