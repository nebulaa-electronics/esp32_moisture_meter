
#include "rgb_led.h"

void run_led(void * para)
{
  for(;;){
    FastLED.show();
    sinelon();
    vTaskDelay(5/portTICK_PERIOD_MS);
  }
}

void rgb_led_setup() {
  // tell FastLED about the LED strip configuration
  //FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  //FastLED.addLeds<LED_TYPE,DATA_PIN,CLK_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);
  xTaskCreate(run_led,"LED",1000,NULL,1,&run_led_handle);
  if(run_led_handle != NULL)
  {
    vTaskSuspend(run_led_handle);
  }
}

//void run_led()
//{
//  FastLED.show();
//  sinelon();
//}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 45, 0, NUM_LEDS-1 );
  leds[pos] += CRGB ( 0, 0, 255);
}

void suspend_led()
{
  if(run_led_handle != NULL)
  {
    vTaskSuspend(run_led_handle);
  }
}
void resume_led()
{
  if(run_led_handle != NULL)
  {
    vTaskResume(run_led_handle);
  }
}
