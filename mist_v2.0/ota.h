#ifndef OTA_H
#define OTA_H

#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include "mm_eeprom.h"

char ssid[]="";
char password[]="";
char ip[]="";

AsyncWebServer server(80);

void ota_setup();
#endif
