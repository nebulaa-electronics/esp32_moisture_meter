#ifndef BATTERY_H
#define BATTERY_H

#include "esp_adc_cal.h"
 
#define batt    35
 
int batt_Result = 0;
float batt_Voltage = 0.0;
float battery;
const float battery_max = 8.40; //maximum voltage of battery
const float battery_min = 6.4;  //minimum voltage of battery before shutdown

float batt_per();
float batt_volt();
uint32_t battADC_Cal(int ADC_Raw);
float mapfloat(double x, double in_min, double in_max, double out_min, double out_max);
int batt_Avg();


#endif
