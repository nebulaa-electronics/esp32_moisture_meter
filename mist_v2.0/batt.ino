#include "batt.h"

float batt_volt()
{
  batt_Result = batt_Avg ();
  batt_Voltage = battADC_Cal(batt_Result);
  battery = mapfloat(batt_Voltage,0,2.686,0,8.4);
  return battery/1000.0;
}

float batt_per()
{
  return ((batt_volt() - battery_min) / (battery_max - battery_min)) * 100;;
}
 
uint32_t battADC_Cal(int ADC_Raw)
{
  esp_adc_cal_characteristics_t adc_chars;
  
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars);
  return(esp_adc_cal_raw_to_voltage(ADC_Raw, &adc_chars));
}

float mapfloat(double x, double in_min, double in_max, double out_min, double out_max)
{
  return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
}

int batt_Avg ()
{
  unsigned int total=0;
  for(int n=0; n<32; n++ )
    total += analogRead(batt);
  return total/32;
}
