#include "mm_eeprom.h"
#include "ota.h"

void eeprom_init()
{
  EEPROM.begin(EEPROM_SIZE);
}

void writeString(char add,String data)
{
  int _size = data.length();
  int i;
  for(i=0;i<_size;i++)
  {
    EEPROM.write(add+i,data[i]);
  }
  EEPROM.write(add+_size,'\0');   //Add termination null character for String Data
  EEPROM.commit();
}


String read_String(char add)
{
  int i;
  char data[100]; //Max 100 Bytes
  int len=0;
  unsigned char k;
  k=EEPROM.read(add);
  while(k != '\0' && len<500)   //Read until null character
  {    
    k=EEPROM.read(add+len);
    data[len]=k;
    len++;
  }
  data[len]='\0';
  return String(data);
}

void set_wifi()
{
  while(flag == false)
  {
//    if (rxValue.endsWith("\n"))
//    {
      delimiter = rxValue.indexOf(",");
      delimiter_1 = rxValue.indexOf(",", delimiter + 1);
      delimiter_2 = rxValue.indexOf(",", delimiter_1 +1);
      y = rxValue.substring(delimiter + 1, delimiter_1);
      z = rxValue.substring(delimiter_1 + 1, delimiter_2);
      writeString(10, z);  //Address 10 and String type data
      writeString(30, y);  //Address 10 and String type data
      essid=read_String(10).c_str();
      epass = read_String(30).c_str();
      WiFi.begin(essid.c_str(), epass.c_str());
//      flag=true;
//    }
      delay(1000);
      wifi_count++;
      //Serial.println(wifi_count);
      if(wifi_count>10)
      {
        wifi_count=0;
        flag=true;
      }
  }
  flag=false;
}
