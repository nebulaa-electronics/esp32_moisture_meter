#include "lm35.h"
#include "esp_adc_cal.h"
#include "ble.h"


float lm35_out()
{
  LM35_Raw_Sensor1 = analogAvg();  
  Voltage = readADC_Cal(LM35_Raw_Sensor1);
  LM35_TempC_Sensor1 = Voltage / 10; 
  return LM35_TempC_Sensor1;
}

int analogAvg ()
{
  unsigned int total=0;
  for(int n=0; n<32; n++ )
    total += analogRead(LM35_Sensor1);
  return total/32;
}

uint32_t readADC_Cal(int ADC_Raw)
{
  esp_adc_cal_characteristics_t adc_chars;
  
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars);
  return(esp_adc_cal_raw_to_voltage(ADC_Raw, &adc_chars));
}
