#ifndef FDC_H
#define FDC_H


#include <Wire.h>
#include "FDC2214.h"


#define CH_NO 0
#define REF_FREQ              40000000.0
#define DIVISOR               268435456.0
#define INDUCTOR              0.000018
#define C                     33
#define VOLUME                92.130503

FDC2214 capsense(FDC2214_I2C_ADDR_0); // Use FDC2214_I2C_ADDR_1 

double F_sensor_x=0;

double raw_code;
double capacitance;
volatile double C_sensor=0;

float C_measured;
float bulk_dens;
float grain_weight;

void fdc_init();
float total_cap();
float bulk_density();
void original_cap();


#endif
