#include "load_cell.h"
#include "ble.h"
#include "mm_eeprom.h"


void live_weight(void* para)
{
  for(;;)
  {
    load_cell_weight();
    vTaskDelay(5/portTICK_PERIOD_MS);
  }
}

void load_cell_init()
{
  EEPROM.get(calVal_eepromAdress, calibrationValue);
  LoadCell.begin();
  LoadCell.start(stabilizingtime, _tare);
  if (LoadCell.getTareTimeoutFlag()) {
    char_print("Timeout,check wiring ");
  }
  else {
    LoadCell.setCalFactor(calibrationValue); // set calibration factor (float)
    char_print("Load Cell is OK!!");
  }
  while (!LoadCell.update());
  char_print("Calibration value: ");
  float_print(LoadCell.getCalFactor());
  xTaskCreate(live_weight,"LIVE_WEIGHT",3000,NULL,1,&load_cell_handle);
  resume_loadcell();
}


void load_cell_calibrate()
{
  char_print("Start calibration");
  char_print("Send 't' to tare offset.");

  boolean _resume = false;
  while (_resume == false) {
    LoadCell.update();
    if(rxValue == "T" || rxValue == "t")
    {
      LoadCell.tare();
    }
    if (LoadCell.getTareStatus() == true) {
      char_print("Tare complete");
      _resume = true;
    }
  }

  char_print("Now, place known weight");
  char_print("Then press send");

  float known_mass = 0;
  _resume = false;
  while (_resume == false) {
    LoadCell.update();
    known_mass = rxValue.toFloat();
      if (known_mass != 0) {
        char_print("Known mass is: ");
        float_print(known_mass);
        Serial.println(known_mass);
        _resume = true;
      }      
  }
  //LoadCell.refreshDataSet(); //refresh the dataset to be sure that the known mass is measured correct
  //Serial.println("loop end");
  float newCalibrationValue = LoadCell.getNewCalibration(known_mass); //get the new calibration value
  
  char_print("New calibration value is: ");
  float_print(newCalibrationValue);
  char_print("Save this value to EEPROM adress ");
  float_print(calVal_eepromAdress);
  char_print("? y/n");

  _resume = false;
  while (_resume == false) {
      if (rxValue == "y") {
        EEPROM.put(calVal_eepromAdress, newCalibrationValue);
        EEPROM.commit();
        EEPROM.get(calVal_eepromAdress, newCalibrationValue);
        char_print("Value ");
        float_print(newCalibrationValue);
        char_print(" saved to EEPROM address: ");
        float_print(calVal_eepromAdress);
        _resume = true;
      }
      else if (rxValue == "n") {
        char_print("Value not saved to EEPROM");
        _resume = true;
      }
  }
  char_print("End calibration");
}

void load_cell_tare()
{
   LoadCell.tare();
  if (LoadCell.getTareStatus() == true) {
    char_print("Tare complete");
  }
}

float load_cell_weight()
{
  static boolean newDataReady = 0;
  const int SerialBTPrintInterval = 800; //increase value to slow down SerialBT print activity

  // check for new data/start next conversion:
  if (LoadCell.update()) newDataReady = true;

  // get smoothed value from the dataset:
  if (newDataReady) {
    if (millis() > t + SerialBTPrintInterval) {
      i = LoadCell.getData();
      if(i < 0.0)
      {
        i = 0;
      }
      newDataReady = 0;
      t = millis();
      
    }
    return i;
  }
}

void suspend_loadcell()
{
  if(load_cell_handle != NULL)
  {
    vTaskSuspend(load_cell_handle);
  }
}
void resume_loadcell()
{
  if(load_cell_handle != NULL)
  {
    vTaskResume(load_cell_handle);
  }
}
