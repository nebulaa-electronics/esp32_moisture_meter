#ifndef LOADCELL_H
#define LOADCELL_H

#include <HX711_ADC.h>


TaskHandle_t  load_cell_handle = NULL;

//pins:
const int HX711_dout = 18; //mcu > HX711 dout pin
const int HX711_sck = 19; //mcu > HX711 sck pin

//HX711 constructor:
HX711_ADC LoadCell(HX711_dout, HX711_sck);

const int calVal_eepromAdress = 0;
unsigned long t = 0;
float calibrationValue; // calibration value
unsigned long stabilizingtime = 2000; // tare preciscion can be improved by adding a few seconds of stabilizing time
boolean _tare = true; //set this to false if you don't want tare to be performed in the next step
float i;

void load_cell_init();
void load_cell_calibrate();
float load_cell_weight();
void load_cell_tare();
void live_weight(void * para);
void suspend_loadcell();
void resume_loadcell();

#endif
