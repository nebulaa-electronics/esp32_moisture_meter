#ifndef BLE_H
#define BLE_H


#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

TaskHandle_t  ble_handle = NULL;

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
BLECharacteristic * pRxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;

std::string rx;
String rxValue;

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"


class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };
    
    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      rx = pCharacteristic->getValue();
      if (rx.length() > 0) {
        for (int i = 0; i < rx.length(); i++)
            rx[i];
      }
      rxValue = rx.c_str();
    }
};


void check_ble(void *para);
void ble_init();
void crop_data(float , float , float);
void char_print(char* );
void float_print(float );
void serial_print();
void suspend_ble();
void resume_ble();
#endif
