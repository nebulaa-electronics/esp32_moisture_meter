#ifndef LM35_H
#define LM35_H

#define LM35_Sensor1    34
 
int LM35_Raw_Sensor1 = 0;
float LM35_TempC_Sensor1 = 0.0;
float Voltage = 0.0;

float lm35_out();
int analogAvg ();
uint32_t readADC_Cal(int ADC_Raw);

#endif
